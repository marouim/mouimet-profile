# Martin Ouimet Profile

### Step 1 - Git clone the repository

```
git clone https://gitlab.com/marouim/mouimet-profile.git
```

### Step 2 - Build Docker images using docker-compose

If you don't have docker-compose installed, please follow the instructions from the
following link.

https://docs.docker.com/compose/install/

```
cd mouimet-profile
docker-compose build
```

### Step 3 - Start the app with docker-compose

Stay in mouimet-profile directory
```
docker-compose up
```

### Step 4 - View my profile

You can now access my profile using your web browser

http://localhost:8002

