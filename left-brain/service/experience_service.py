from pymongo import MongoClient
import os
import uuid


class ExperienceService:

    def __init__(self):
        self.client = MongoClient(os.environ["MONGO_HOST"], int(os.environ["MONGO_PORT"]))
        self.profile_db = self.client.profile

    def __del__(self):
        self.client.close()

    def get_experience_list(self):
        experience_col = self.profile_db.experience

        to_return = []
        for exp in experience_col.find():
            exp.pop('_id')
            to_return.append(exp)

        return to_return

    def create_experience(self, experience):
        experience_col = self.profile_db.experience

        experience['id'] = uuid.uuid4()
        new_experience_id = experience_col.insert_one(experience)
        new_experience = experience_col.find_one({'_id': new_experience_id.inserted_id})

        new_experience.pop('_id')

        return new_experience







