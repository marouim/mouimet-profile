from flask import jsonify, request
from flask_restplus import Namespace, Resource
from service.experience_service import ExperienceService


api = Namespace('application', description='Application API')


@api.route("")
class ViewExperience(Resource):

    def __init__(self, api):
        Resource.__init__(self, api)
        self.experience_service = ExperienceService()

    def get(self):
        return jsonify(self.experience_service.get_experience_list())

    def post(self):
        return jsonify(self.experience_service.create_experience(request.get_json()))

