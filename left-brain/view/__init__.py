from flask_restplus import Api
from experience_view import api as experience_api


api = Api(
    title='Martin Ouimet Profile backend API',
    version='0.1',
    description='Martin Ouimet Profile backend API',
    doc='/apidoc/'
)

api.add_namespace(experience_api, path='/experience')
