// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'


Vue.config.productionTip = false

// Vuetify
import Vuetify from 'vuetify'
Vue.use(Vuetify, {
    theme: {
        primary: '#00549a',
        secondary: '#003778',
        accent: '#20a0ff',
        error: '#ff4949'
    }
})
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
