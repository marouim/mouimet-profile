"use strict"


const skillService = require('../services/skillService')

const skillController = {

    getSkillList(req, res) {
        return skillService.getSkillList()
    }
}

module.exports = skillController

