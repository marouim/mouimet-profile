"use strict"


const skillController = require('../controllers/skillController')

const skillRoutes = (app) => {

    app.route('/skills')
        .get(skillController.getSkillList)

}

module.exports = skillRoutes
