'use strict';
let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let skillSchema = new Schema({
    skillId: {
        type: String,
        required: ''
    },
})

module.exports = mongoose.model('skill', skillSchema)