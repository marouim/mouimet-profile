//
// Entrypoint
//

"use strict";

// Environment variable check
console.log("Environment variable check:")

// Build MongoDB connection string
const mongoURI = "mongodb://" +
    process.env.MONGO_HOST + ":" +
    process.env.MONGO_PORT + "/" +
    process.env.MONGO_DATABASE

console.log("MongoDB URI: " + mongoURI)

const express = require('express')
const app = express()
const mongoose = require('mongoose')


// Mongo Models
// Declare variables to trigger Schema loading.
const skillModel = require('./api/models/skillModel')


// Connect to MongoDB
let mongooseOptions = {
    useMongoClient: true
}
mongoose.Promise = global.Promise;
mongoose.connect(mongoURI, mongooseOptions, (err) => {
    if (err) {
        console.log("Cannot connect to mongo. Stop process.")
        process.exit(1)
    }
});

// Parser for request body in JSON
const bodyParser = require('body-parser')

// Adjusted ParameterLimit, limit and json bodyparser limit
// because of PayloadTooLargeError error when huge payload
// https://stackoverflow.com/questions/19917401/error-request-entity-too-large
app.use(bodyParser.urlencoded(
    {
        limit: '50mb',
        extended: true,
        parameterLimit:50000
    }
));
app.use(bodyParser.json({
    limit: '50mb'
}));

// Add API routes
const skillRoutes = require('./api/routes/skillRoute')

skillRoutes(app)

// Start app
app.listen(3000, () => console.log('Listening on port 3000!'))
